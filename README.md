# Python PDF Examples

Examples of PDF creation using different Python libraries/modules.

## Play with me

This repository provides examples using the following libraries/modules to create PDF files:

- [ReportLab](https://www.reportlab.com/software/opensource/rl-toolkit/)
- [FPDF2](https://pyfpdf.github.io/fpdf2/index.html)

### Prerequisites

- Python ([version](.python-version))
- Virtual env management (e.g. [pyenv](<https://github.com/pyenv/pyenv>))

### Run Locally

```bash
# switch to project root directory and setup a virtual environment (e.g. pyenv)
pyenv virtualenv <ENV_NAME>

# activate virtual environment
pyenv activate <ENV_NAME>

# install the project requirements
pip install -r requirements.txt

# run application
python create_pdf.py <module>
```