import click

from ppe.fpdf2 import gen_pdf as gen_fpdf2_pdf
from ppe.reportlab import gen_pdf as gen_reportlab_pdf


@click.group()
def cli():
    pass


@click.command()
@click.option(
    '-o', '--output',
    is_flag=False,
    flag_value='reportlab-example.pdf',
    default='reportlab-example.pdf',
    help='Output file',
)
def reportlab(output):
    '''
    Creates a PDF files using the library/module ReportLab.
    '''
    click.echo('Creating PDF file using ReportLab module...')
    gen_reportlab_pdf(output)
    click.echo('PDF file created.')


@click.command()
@click.option(
    '-o', '--output',
    is_flag=False,
    flag_value='fpdf2-example.pdf',
    default='fpdf2-example.pdf',
    help='Output file',
)
def fpdf2(output):
    '''
    Creates a PDF files using the library/module PyFPDF2.
    '''
    click.echo('Creating PDF file using PyFPDF2 module...')
    gen_fpdf2_pdf(output)
    click.echo('PDF file created.')


cli.add_command(reportlab)
cli.add_command(fpdf2)


if __name__ == '__main__':
    cli()
    