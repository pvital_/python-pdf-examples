import os
import pathlib

from fpdf import FPDF


def gen_pdf(output_file: str = None) -> None:
    '''
    Creates a new PDF file names as output_file using an image as background.
    '''
    pdf = FPDF(orientation="P", unit="mm", format="A4")
    pdf.add_page()

    # Read image to use as background and draw it
    pdf.image(
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            './samples/Monthly-Budget-Form-Template.png'
        ), 
        x=0, y=0
    )

    # Set font to Helvetica Bold with 16-point size
    pdf.set_font('times', 'B', 12)

    # Write (add new cell)
    pdf.set_xy(42,236)
    # print(pdf.get_x(), pdf.get_y())
    pdf.cell(105, 5, txt='Sample text')
    pdf.cell(40, 5, txt='$ 3,500.00', align="C", ln=1)

    pdf.set_xy(42,244)
    pdf.cell(105, 5, txt='Sample text 2')
    pdf.cell(40, 5, txt='$ 5,000.00', align="C", ln=1)
    # pdf.cell(40, 10, txt='$ 5,000.00', align="C", border=1, ln=1)

    # Save the PDF file
    pdf.output(output_file)


if __name__=='__main__':
    output = 'fpdf2-example.pdf'
    gen_pdf(output)