import os
import pathlib

from reportlab.lib.colors import blue, red
from reportlab.lib.pagesizes import A4
from reportlab.lib.utils import ImageReader
from reportlab.lib.units import inch

from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfgen.canvas import Canvas


def gen_pdf(output_file: str = None) -> None:
    '''
    Creates a new PDF file names as output_file using an image as background.
    '''
    canvas = Canvas(output_file, pagesize=A4)

    # Read image to use as background and draw it
    img = ImageReader(
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            './samples/Monthly-Budget-Form-Template.png'
        )
    )
    canvas.drawImage(img, 0, 0, anchor='sw', anchorAtXY=True)

    # Set font to Times New Roman with 12-point size
    canvas.setFont('Times-Roman', 12)
    
    # If you want load a different font than those provided by ReportLab, uncomment the following
    # registerFont(TTFont('arial','C:\\windows\\fonts\\arial.ttf'))
 
    # Draw blue text 1.7 inch from the left (x = 0) and 2.25 inches from the bottom (y = 0)
    canvas.setFillColor(blue)
    canvas.drawString(1.7 * inch, 2.25 * inch, 'Blue text')

    # Draw red text 6.3 inch from the left (x = 0) and 2.25 inches from the bottom (y = 0)
    canvas.setFillColor(red)
    canvas.drawString(6.3 * inch, 2.25 * inch, 'Red text')

    # Save the PDF file
    canvas.save()

if __name__=='__main__':
    output = 'reportlab-example.pdf'
    gen_pdf(output)